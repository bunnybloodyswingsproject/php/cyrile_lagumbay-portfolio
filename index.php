<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8">
   <meta name="description" content="online shopping website for fashion clothes, customized cakes, and jewelry">
   <link rel="shortcut icon" type="image/png" href="./assets/images/c-logo-removebg-preview.png">
   <title>Cyrile Lagumbay</title>

   <!-- Metatags -->

   <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0">

   <!-- Fontawesome -->
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
   <script src="https://use.fontawesome.com/1a8df02521.js"></script>
   <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

   <!-- Google Fonts -->
   <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&family=Potta+One&display=swap" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css2?family=Hanalei&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">

   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

   <!-- Custom CSS -->
   <link rel="stylesheet" type="text/css" href="./assets/css/style.css">
</head>
<body>
   <div class="wrapper"> 
   <!-- Header -->

      <header class="header-1">
         <nav class="nav">
            <div class="title_header">
               <div class="title_here">
                  <a href="#">
                     <img src="./assets/images/c-logo-removebg-preview.png">
                  </a>
                  <h4>Cyrile Lagumbay</h4>
               </div>

               <div class="role">
                  <p>Data Analyst</p>
               </div>

               <div class="nav_content">
                  <p>menu</p>
                  <div class="menu-btn">
                     <div class="menu-btn_burger"></div>
                  </div>
                  <div class="nav_menu" id="nav_menu">
                     <ul class="nav_list">
                        <li class="nav_item">
                           <a href="#" class="nav_link" id="#">Home</a>
                        </li>
                        <li class="nav_item">
                           <a href="#testimony" class="nav_link">TESTIMONIES</a>
                        </li>
                        <li class="nav_item">
                           <a href="#project" class="nav_link">PROJECT</a>
                        </li>
                        <li class="nav_item">
                           <a href="#methodology" class="nav_link">METHODOLOGY</a>
                        </li>
                        <li class="nav_item">
                           <a href="#perspective" class="nav_link">PERSPECTIVE</a>
                        </li>
                        <li class="nav_item">
                           <a href="#aboutme" class="nav_link">ABOUT ME</a>
                        </li>
                        <li class="nav_item">
                           <a href="#contact" class="nav_link">CONTACT</a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </nav>
      </header>

      <section class="home">
         <div class="home_container">
            <div class="home_content">
               <div class="home_image">
                  <h2 class="text text-dark">
                     <div class="parallax-title">
                        WEB AND FULL STACK DEVELOPER DATA ANALYST
                     </div>
                  </h2>

                  <h2 class="second-text text-dark">
                     <div class="parallax-title-reverse">
                        WEB AND FULL STACK DEVELOPER DATA ANALYST
                     </div>
                  </h2>
               </div>
               <h2 class="text">
                  <div class="parallax-title">
                     WEB AND FULL STACK DEVELOPER DATA ANALYST
                  </div>
               </h2>
               <h2 class="second-text">
                  <div class="parallax-title-reverse">
                     WEB AND FULL STACK DEVELOPER DATA ANALYST
                  </div>
               </h2>
            </div>
         </div>
      </section>

      <section class="testimonies" id="testimony">
         <div class="testimony_container">
            <div class="testimony_content">
               <div class="stats_intro">
                  <h4>Check my programming stats</h4>
                  <div class="stats_logo" onmouseover="removeArrow(this)" onmouseout="normalLogo(this)">
                     <i class="fas fa-location-arrow"></i>
                     <i class="fas fa-chart-bar"></i>
                  </div>
               </div>

               <div class="image_slider">
                  <div class="slider">
                     <div class="slides">
                        <!-- Radio Buttons -->
                        <input type="radio" name="radio-btn" id="radio1">
                        <input type="radio" name="radio-btn" id="radio2">
                        <input type="radio" name="radio-btn" id="radio3">
                        <input type="radio" name="radio-btn" id="radio4">

                        <!-- Slider images -->
                        <div class="slide first">
                           <img src="./assets/images/blackCanvas.jpg">
                           <div class="slide_description">
                              <div class="messageQuotation">
                                 <i class="fas fa-quote-left"></i>
                              </div>
                              <div class="testimonial_person">
                                 <h4>Andy Ng</h4>
                                 <p>Risk and Compliance <br>Supervisor</p>
                                 <p>Red Pixel Solution Global Inc.</p>
                              </div>
                              <div class="testimonial_message">
                                 <p>Pleasure to work with you.<br> You have a great analytical skills that surpass mine. <br>Good and happy to work with.
                                 <br>
                                 looking forward on your journey as a
                                 <br> risk and compliance officer.
                                 </p>
                              </div>
                           </div>
                        </div>

                        <div class="slide">
                           <img src="./assets/images/blackCanvas.jpg">
                           <div class="slide_description">
                              <div class="messageQuotation">
                                 <i class="fas fa-quote-left"></i>
                              </div>
                              <div class="testimonial_person">
                                 <h4>Pia Marni Opulencia</h4>
                                 <p>Owner and Founder</p>
                                 <p>Sweet Counter Bake Shop</p>
                              </div>
                              <div class="testimonial_message">
                                 <p>The prototype website that you've created <br>for us will be the suitable platform <br> for our own small shop. <br> We are looking forward on making <br>this prototype to a reality.
                                 </p>
                              </div>
                           </div>
                        </div>

                        <div class="slide">
                           <img src="./assets/images/blackCanvas.jpg">
                           <div class="slide_description">
                              <div class="messageQuotation">
                                 <i class="fas fa-quote-left"></i>
                              </div>
                              <div class="testimonial_person">
                                 <h4>Leah De Guzman</h4>
                                 <p>Owner and Founder</p>
                                 <p>SAYUGI Clothing and Accessories</p>
                              </div>
                              <div class="testimonial_message">
                                 <p>Great layout!! You open a new possible marketing <br>strategy for us to start our business. <br> We will consider this prototype whenever <br> we decide on continuing our business <br> strategy to the next level
                                 </p>
                              </div>
                           </div>
                        </div>

                        <div class="slide">
                           <img src="./assets/images/3.jpg">
                        </div>

                        <!-- Automatic navigation start -->
                        <div class="navigation-auto">
                           <div class="auto-btn1"></div>
                           <div class="auto-btn2"></div>
                           <div class="auto-btn3"></div>
                           <div class="auto-btn4"></div>
                        </div>

                        <!-- Manual navigation start -->
                        <div class="navigation-manual">
                           <label for="radio1" class="manual-btn"></label>
                           <label for="radio2" class="manual-btn"></label>
                           <label for="radio3" class="manual-btn"></label>
                           <label for="radio4" class="manual-btn"></label>
                        </div>
                     </div>
                  </div>
               </div>
               
               <div class="programming_stats">
                  <h4 class="stats_title">Programming Stats</h4>
                  <div class="programming_logo">
                     <ul class="skills">
                        <li class="skill_lists">
                           <img src="./assets/images/HTML-removebg-preview.png">
                           <div class="skill_rates">
                              <p>10/10</p>
                              <div class="stars">
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                              </div>
                           </div>
                        </li>
                        <li class="skill_lists">
                           <img src="./assets/images/css-removebg-preview.png">
                           <div class="skill_rates">
                              <p>8/10</p>
                              <div class="stars">
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                              </div>
                           </div>
                        </li>
                        <li class="skill_lists">
                           <img src="./assets/images/javascript-removebg-preview.png">
                           <div class="skill_rates">
                              <p>7/10</p>
                              <div class="stars">
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                              </div>
                           </div>
                        </li>
                        <li class="skill_lists">
                           <img src="./assets/images/mysql-removebg-preview.png">
                           <div class="skill_rates">
                              <p>7/10</p>
                              <div class="stars">
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                              </div>
                           </div>
                        </li>
                        <li class="skill_lists">
                           <img src="./assets/images/php.png">
                           <div class="skill_rates">
                              <p>7/10</p>
                              <div class="stars">
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                              </div>
                           </div>
                        </li>
                        <li class="skill_lists">
                           <img src="./assets/images/vba.png">
                           <div class="skill_rates">
                              <p>8/10</p>
                              <div class="stars">
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                              </div>
                           </div>
                        </li>
                        <li class="skill_lists">
                           <img src="./assets/images/java-removebg-preview.png">
                           <div class="skill_rates">
                              <p>7/10</p>
                              <div class="stars">
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                              </div>
                           </div>
                        </li>
                        <li class="skill_lists">
                           <img src="./assets/images/bootstrap-removebg-preview.png">
                           <div class="skill_rates">
                              <p>8/10</p>
                              <div class="stars">
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                              </div>
                           </div>
                        </li>
                        <li class="skill_lists">
                           <img src="./assets/images/git.png">
                           <div class="skill_rates">
                              <p>8/10</p>
                              <div class="stars">
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                                 <i class="fas fa-star"></i>
                              </div>
                           </div>
                        </li>
                     </ul>
                  </div>
                  <div class="stats_close">
                     <i class="far fa-times-circle"></i>
                     <p>Close</p>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="projects" id="project">
         <div class="project_container">
            <div class="project_content">
               <div class="section_title">
                  <h2 class="title_1">PROJECTS</h2>
                  <h2 class="title_2">PROJECTS</h2>
               </div>

               <div class="projects_here">
                  <div class="projects_row">
                     <div class="project_box">
                        <div class="imageBox">
                           <a href="#">
                              <img src="assets/images/project_image.png">
                           </a>
                        </div>
                        <div class="contentBox">
                           <h2>SAYUJI</h2>
                           <p>An E-commerce website built using PHP and MYSQL only. The pages were comprised of HTML for skeleton, CSS for the design, and Javascript for after event effects of the elements.</p>
                        </div>
                     </div>
                  </div>

                  <div class="projects_row">
                     <div class="project_box">
                        <div class="imageBox">
                           <a href="#">
                              <img src="assets/images/project_image2.png">
                           </a>
                        </div>
                        <div class="contentBox">
                           <h2>CL</h2>
                           <p>An E-commerce website built with only Javascript. This is for practicing the power of Javascript to build a website using Node.js in the future.</p>
                        </div>
                     </div>
                  </div>

                  <div class="projects_row">
                     <div class="project_box">
                        <div class="imageBox">
                           <a href="#">
                              <img src="assets/images/project_conversion.png">
                           </a>
                        </div>
                        <div class="contentBox">
                           <h2>Funding and Settlement Conversion Rate</h2>
                           <p>A simple VBA Macro tool for converting 6 currencies that the funding and settlement team assigned me to do.</p>
                        </div>
                     </div>
                  </div>

                  <div class="projects_row">
                     <div class="project_box">
                        <div class="imageBox">
                           <a href="#">
                              <img src="assets/images/project_vba.png">
                           </a>
                        </div>
                        <div class="contentBox">
                           <h2>Backend Userlist</h2>
                           <p>A simple VBA Macro tool for my record to track all the active and disabled user of the company backend website.</p>
                        </div>
                     </div>
                  </div>

                  <div class="projects_row">
                     <div class="project_box">
                        <div class="imageBox">
                           <a href="#">
                              <img src="assets/images/otherProjects.png">
                           </a>
                        </div>
                        <div class="contentBox">
                           <h2>Other Small Projects</h2>
                           <p>Here are the compilation of small projects that ive accomplished as a Risk and Compliance Administrator in my previous companies.</p>
                        </div>
                     </div>
                  </div>

                  <div class="projects_row">
                     <div class="project_box">
                        <div class="imageBox">
                           <a href="#">
                              <img src="assets/images/pathfinder.png">
                           </a>
                        </div>
                        <div class="contentBox">
                           <h2>Breadth-Tree Search Pathfinder</h2>
                           <p>This project is one of many pathfinding algorithm that you can see in the world. The function of this is to find the possible nearest path in the maze.</p>
                        </div>
                     </div>
                  </div>

               </div>
            </div>
         </div>
      </section>
      
      <section class="methodologies" id="methodology">
         <div class="methodology_container">
            <div class="methodology_content">
               <div class="method_title method1">
                  <h2 class="title_1">METHODOLOGY</h2>
                  <h2 class="title_2">METHODOLOGY</h2>
               </div>

               <div class="method_title method2">
                  <h2 class="title_1">METHODS</h2>
                  <h2 class="title_2">METHODS</h2>
               </div>

               <div class="method_container bd-grid">
                  <div class="method_box">
                     <div class="method_logo">
                        <i class="fas fa-pencil-alt"></i>
                     </div>
                     <h4>Step 1</h4>
                     <p>Construct the idea using Figma</p>
                  </div>

                  <div class="method_box">
                     <div class="method_logo">
                        <i class="fas fa-table"></i>
                     </div>
                     <h4>Step 2</h4>
                     <p>Draft the possible ERD and tables of the project</p>
                  </div>

                  <div class="method_box">
                     <div class="method_logo">
                        <i class="fab fa-html5"></i>
                     </div>
                     <h4>Step 3</h4>
                     <p>Layout the initial pages with HTML</p>
                  </div>

                  <div class="method_box">
                     <div class="method_logo">
                        <i class="fab fa-css3-alt"></i>
                     </div>
                     <h4>Step 4</h4>
                     <p>Construct the basic design using CSS</p>
                  </div>

                  <div class="method_box">
                     <div class="method_logo">
                        <i class="fab fa-js-square"></i>
                     </div>
                     <h4>Step 5</h4>
                     <p>Solidify every design using JS and connect every possible API using asynchronous operation </p>
                  </div>

                  <div class="method_box">
                     <div class="method_logo">
                        <i class="fas fa-mobile-alt"></i>
                     </div>
                     <h4>Step 6</h4>
                     <p>Apply all design in different responsive medium layout</p>
                  </div>

                  <div class="method_box">
                     <div class="method_logo">
                        <i class="fas fa-paint-brush"></i>
                     </div>
                     <h4>Step 7</h4>
                     <p>Finishing touches</p>
                  </div>

                  <div class="method_box">
                     <div class="method_logo">
                        <i class="fas fa-globe-americas"></i>
                     </div>
                     <h4>Step 8</h4>
                     <p>Push the project</p>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="perspectives" id="perspective">
         <div class="perspective_container">
            <div class="perspective_content">
               <div class="section_title perspective_title">
                  <h2 class="title_1">PERSPECTIVE</h2>
                  <h2 class="title_2">PERSPECTIVE</h2>
               </div>

               <div class="perspective_box">
                  <div class="perspective_expertise">
                     <h2>Expertise</h2>
                     <br>
                     <p class="perspective_category">Responsive Ecommerce</p>
                     <br>
                     <p class="perspective_description">I always make sure that my projects will be use in different mediums to show the scalability of the design and its functionality.</p>
                  </div>
                  <div class="responsive_images">
                     <div class="reponsive_1">
                        <img src="./assets/images/mobile_version-removebg-preview.png">
                     </div>
                     <div class="reponsive_2">
                        <img src="./assets/images/ipad_version.png">
                     </div>
                     <div class="reponsive_3">
                        <img src="./assets/images/desktop_version-removebg-preview.png">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="netflix" id="aboutme">
         <div class="blackBackground"></div>
         <div class="netflix_img">
            <img src="./assets/images/herry-sucahya-oPVohQ1rTVA-unsplash.jpg">
         </div>
         <div class="netflix_container">
            <div class="netflix_content">
               <div class="netflix_seperator"></div>
               <div class="netflix_box">
                  <div class="n_series">
                     <h2>N</h2>
                     <h4>Series</h4>
                  </div>
                  <div class="series_title">
                     <h1>CYRILE <br>Lagumbay</h1>
                  </div>
                  <div class="netflix_rating">
                     <p class="n_percent">100% Passionate</p>
                     <p class="n_course">BS Psychology</p>
                  </div>
                  <div class="netflix_logo">
                     <i class="fab fa-codepen"></i>
                     <p>#Bootcamp Graduate</p>
                  </div>
                  <div class="netflix_description">
                     <p>A story of the boy who fall in love in the ways how codes were make. Not taking his original degree destination, he is how a professional data analyst who embarks on a journey to attain the destination of being one of the best in his field.</p>
                  </div>
               </div> 
            </div>
         </div>
      </section>

      <section class="callToAction" id="contact">
         <div class="call_container">
            <div class="call_content">
               <div class="call_title">
                  <h2 class="title_1">CONTACT ME!</h2>
                  <h2 class="title_2">CONTACT ME!</h2>
               </div>

               <div class="action_container">
                  <div class="action_box">
                     <div class="action_titles">
                        <i class="fas fa-hand-peace"></i>
                        <i class="far fa-hand-point-right removeElement"></i>
                        <h3>EMAILS</h3>
                     </div>
                     <ul class="dropDownList removeElement dropDown1st">
                        <li class="dropDown1">
                           <i class="fas fa-envelope-open-text"></i>
                           <a href="#" class="dropDownLinks">cyrilelagumbay@gmail.com</a>
                        </li>
                     </ul>
                  </div>

                  <div class="action_box">
                     <div class="action_titles">
                        <i class="fas fa-hand-peace"></i>
                        <i class="far fa-hand-point-right removeElement"></i>
                        <h3>PHONE NUMBER</h3>
                     </div>
                     <ul class="dropDownList removeElement dropDown2nd">
                        <li class="dropDown1">
                           <i class="fas fa-phone-alt"></i>
                           <a href="#" class="dropDownLinks">0926-890-3301</a>
                        </li>
                     </ul>
                  </div>

                  <div class="action_box">
                     <div class="action_titles">
                        <i class="fas fa-hand-peace"></i>
                        <i class="far fa-hand-point-right removeElement"></i>
                        <h3>SOCIAL MEDIA</h3>
                     </div>
                     <ul class="dropDownList removeElement dropDown3rd">
                        <li class="dropDown1">
                           <i class="fab fa-facebook-f"></i>
                           <a href="#" class="dropDownLinks">cyrile lagumbay</a>
                        </li>
                        <li class="dropDown1">
                           <i class="fab fa-linkedin-in"></i>
                           <a href="#" class="dropDownLinks">@cyrilelagumbay</a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </div>
   <footer>
      <small>&copy;yrile Lagumbay</small>
   </footer>
   <!-- CDN -->
   <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

   <!-- TweenMax JS -->
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script>
   <!-- Three JS -->
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/three.js/106/three.min.js"></script>
   <!-- Hover JS -->
   <script type="text/javascript" src="./assets/js/hover.js"></script>
   <!-- Custom JS -->
   <script src="./assets/js/script.js"></script>
   <!-- Observer JS -->
   <script src="./assets/js/intersection-observer.js"></script>
   <!-- Vanilla Tilt JS -->
   <script src="./assets/js/VanillaTilt.js"></script>



   <script type="text/javascript">
      VanillaTilt.init(document.querySelectorAll(".project_box"), {
         max: 25,
         speed: 100
      });
      
   </script>
</body>
</html>
